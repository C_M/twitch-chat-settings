import { TYPE } from '../types'

/* 
Must be strings to use in inputs (no solution found yet)
Workaround is the type string
*/
export interface ChannelSettings{
	messageStart: string
	hideMessagesStartingWith: any
	emoteThemeColor: string //"dark" | "light"
	message_color: string
	messageClasses: string
	removed_message_message: string
	removed_message_keep_name: string
	removeAfter:string
	showUserColorWhenMentioned:string 
	showBadgesWhenMentioned:string
	css:string
	permission:any

}

// export function generalSettingsDescription(){
//  const desc =[   {'key': "messageStart", 'type': "s", 'description': "seperator between name and message"},
//     {'key': "hideMessagesStartingWith", 'type': "s[]", 'description': ""},
//     {'key': "emoteThemeColor", 'type': "s", 'description': "Theme color for which emotes to fetch, dark or light"},
//     {'key': "message_color", 'type': "s", 'description': "set color for message text, false for default"},
//     {'key': "messageClasses", 'type': "s", 'description': "Css classes to add to to message"},
//     {'key': "removed_message_message", 'type': "b", 'description': "show this text if message is deleted"},
//     {'key': "removed_message_keep_name", 'type': "b", 'description': "true or false: Keep name of deleted message user"},
//     {'key': "removeAfter", 'type': "int", 'description': "delete messages after N milliseconds"},

//     ]
// }

const permissionSettings: any[] = [
	{'key': "bold", 'type': TYPE.BOOLEAN, 'description': ""},
	{'key': "italic", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "underline", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "strikethrough", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "monospace", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "userColor", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "msgColor", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "fontSize", 'type': TYPE.BOOLEAN,'description': ""},
	{'key': "msgStart", 'type': TYPE.BOOLEAN,'description': ""},

]

export const generalSettingsDescriptionList:GeneralSettingsDescription[] =[   
	{'key': "messageStart", 'type': TYPE.STRING, 'description': "seperator between name and message", settings: null},
    {'key': "hideMessagesStartingWith", 'type': TYPE.STRINGARRY, 'description': "", settings: null},
    {'key': "removeAfter", 'type': TYPE.NUMBER, 'description': "delete messages after N milliseconds", settings: null},
    {'key': "message_color", 'type': TYPE.STRING, 'description': "set color for message text, false for default", settings: null},
    {'key': "showBadgesWhenMentioned", 'type': TYPE.BOOLEAN, 'description': "Show badges when someone is mentioned (@someone)", settings: null},
    {'key': "showUserColorWhenMentioned", 'type': TYPE.BOOLEAN, 'description': "Show users name color when someone is mentioned (@someone)", settings: null},
    {'key': "messageClasses", 'type': TYPE.STRING, 'description': "Css classes to add to to message", settings: null},
    {'key': "emoteThemeColor", 'type': TYPE.STRING, 'description': "Theme color for which emotes to fetch, dark or light", settings: null},
    {'key': "removed_message_message", 'type': TYPE.STRING, 'description': "show this text if message is deleted, false for no message", settings: null},
    {'key': "removed_message_keep_name", 'type': TYPE.BOOLEAN, 'description': "true or false: Keep name of deleted message user", settings: null},
    {'key': "css", 'type': TYPE.STRING, 'description': "add custom css to the chat", settings: null},
    {'key': "permission", 'type': TYPE.CATEGORY, 'description': "set the permission for user groups", settings:[
	    {'key':"standard", 'options': permissionSettings},
	    {'key':"subscriber", 'options': permissionSettings},
	    {'key':"vip", 'options': permissionSettings},
	    {'key':"moderator", 'options': permissionSettings},
	    {'key':"broadcaster", 'options': permissionSettings}
	    ]
	}
	    
    
    ]

export let generalSettingsDescriptionListID:{[key:string]:number}= {}

for(let i = 0; i < generalSettingsDescriptionList.length; ++i){
	generalSettingsDescriptionListID[generalSettingsDescriptionList[i].key] = i;
}

export interface Channel {
	name:string
	id:number
	// settings:{[key:string]:any}
	settings:ChannelSettings
}

export class channelData{
	channels:Channel[] = [];

}


export interface GeneralSettingsDescription{
	key:  "messageStart"|"emoteThemeColor"| "message_color" | "messageClasses" | "removed_message_keep_name" 
	| "removed_message_message" | "removeAfter" | "hideMessagesStartingWith" | "showUserColorWhenMentioned" | 
	"showBadgesWhenMentioned" | "css" | "permission"
	type: TYPE
	description:string
	settings:any;

}

