export class TYPE{


  // Create new instances of the same class as static attributes
  static STRING = new TYPE("STRING")
  static BOOLEAN = new TYPE("BOOLEAN")
  static NUMBER = new TYPE("NUMBER")
  static STRINGARRY = new TYPE("STRINGARRY")
  static CATEGORY = new TYPE("CATEGORY")
  name;


  constructor(name:string) {
    this.name = name
  }
}