import { Injectable } from '@angular/core';
import { Channel } from './model/channel'
import { User } from './model/user'
import { ActivatedRoute } from '@angular/router'
import { NgxIndexedDBService } from 'ngx-indexed-db';


interface subscribers{
	channel:Channel[];
	currentChannel:any[];
	currentChannelID:number;

}


@Injectable({
  providedIn: 'root'
})

export class DataService {

	//channels: {[key:number]:Channel} = {}
	_channels: Channel[] = [];
  _notSaved = false
  //_channelsDict:{[key:number]:Channel} = {};
	_currentChannelId:number = NaN;
	channelByID:{[Key:number]:Channel} = {}
  _channelUsers: User[] | null= null;

	//_currentChannel: Channel | null = null
	_subscribers:{[key:string]:any}={'channels':[], 'currentChannel':[], 'currentChannelID':[], 'notSaved':[]}

  constructor(private dbService:NgxIndexedDBService, private route:ActivatedRoute) {
    this.getAllChannels();
  }

 
  getAllChannels(){
    const self = this
    this.dbService.getAll('channels').subscribe(
        (result:any) => { self.channels=result});
  }

  subscribe(prop:'channels' | 'currentChannel' | 'currentChannelID' | 'notSaved', callback:any):void{
  	this._subscribers[prop].push(callback)
  }

  notifySubscribers(prop:'channels' | 'currentChannel' | 'currentChannelID' | 'notSaved', val:any):void{
  	
  	for(let i = 0; i < this._subscribers[prop].length; ++i){
  		this._subscribers[prop][i](prop, val)
  	}
  }

  set channels(val){
  	this._channels=val;
  	this.notifySubscribers('channels', this._channels)
  	// reset and repopulate the channelById
  	this.channelByID = {}
  	for(let i = 0; i < this._channels.length; ++i){
  		this.channelByID[this._channels[i].id] = this._channels[i]
  	}
    // update currentchannel also
    if(this._currentChannelId){
      this.notifySubscribers('currentChannel', this.currentChannel)
    }

  }

  set currentChannelID(val:number){
    if(val === this._currentChannelId){
      console.log(["new current id is same", this._currentChannelId, val])
      return
    }
    console.log(["update currentID", this._currentChannelId, "=>", val])
  	this._currentChannelId = val
    this._channelUsers = null; // other channel so reset userlist
  	this.notifySubscribers('currentChannelID', val)
    this.notifySubscribers('currentChannel', this.currentChannel)
  }

  get currentChannelID(){
    return this._currentChannelId;
  }


  getUserlist():User[]{
    // let index_datail:IndexDetails ={
    //   name: 'string'
    console.log(["request userlist", this._currentChannelId])
    if(!this._currentChannelId){ // no current channel, so no userss
      return [];
    }
    if (this._channelUsers != null){
      return this._channelUsers
    }
    // @ts-ignore
      return new Promise((resolve) => 
        {
          this.dbService.getAllByIndex(
            'users', 'roomID', IDBKeyRange.only(this._currentChannelId)).subscribe(
              (result:any) => {           
                this._channelUsers = result;
                resolve(this._channelUsers)
                }
              );
        }
      );
  }

  get currentChannel(){
    if(this._channels.length < 1){ // channels not yet loaded, so return null
      return null
    }
    return this.channelByID[this._currentChannelId]
  }

  get channels(){
  	return this._channels;
  }

  setNotSaved(val:boolean, sendAlways=false){
    if(!sendAlways && this._notSaved === val){
      return
    }
    this._notSaved=val
    this.notifySubscribers('notSaved', this._notSaved)
    console.log("dataserver data changed")

  }



  dataChanged = () => this.setNotSaved(true); 

  get isChanged(){ return this._notSaved}

  save(db:string, item:any){
    this.dbService.update(db, item ).subscribe((storeData) => {
       console.log( storeData);
       this.setNotSaved(false, true)
    });
  }
}
