import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
//import { BehaviorSubject, Observable } from 'rxjs';
//import { DatabaseService } from '../database.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { Router } from '@angular/router';
import { Channel } from '../model/channel';
import { DataService } from '../data.service'



@Component({
  selector: 'app-top-bar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})


export class TopBarComponent implements OnInit{
	
	@Input() channels:Channel[] =[]
//	data;
 // @Output() 
 // valueChange = new EventEmitter<Channel[]>();

 @Input() channelChanged = this.dataService.isChanged;

	selectedOption:string = ""
	channel: Channel | null = null
  

	
	constructor( 
		private dbService: NgxIndexedDBService, 
		private router:Router, 


	//	private changeRef: ChangeDetectorRef,
		private dataService:DataService
		){
	
		//this.data = new BehaviorSubject(this.channels);
		//console.log(this.data)
	}




	ngOnInit(){
		const self = this

		this.dataService.subscribe('notSaved', this.changedUpdate.bind(this))
		this.channel = this.dataService.currentChannel
		this.dataService.subscribe('currentChannel', this.currentChannelUpdated.bind(this))


	}

	currentChannelUpdated(prop:string, val:Channel){
		this.channel=this.dataService.currentChannel;
		// if(this.dataService.isChanged){
		// 	this.changedUpdate("init", this.dataService.isChanged)
		// }
	}


	changedUpdate(prop:string, notSaved:boolean){
		if(notSaved){
			//@ts-ignore
			document.querySelector("#topbar-save-button").innerHTML = "Unsaved changed | Save"
			//@ts-ignore
			document.querySelector("#topbar-save-button").classList.add('unsaved') 
			//@ts-ignore
			document.querySelector("#topbar-save-button").classList.remove('saved') 
		}else{
			//@ts-ignore
			document.querySelector("#topbar-save-button").innerHTML = "Saved"
			//@ts-ignore
			document.querySelector("#topbar-save-button").classList.remove('unsaved') 
			//@ts-ignore
			document.querySelector("#topbar-save-button").classList.add('saved') 
		}
		
		//this.valueChange.emit(this.channels)
		//() => this.valueChange.emit(this.channels);
	//	this.changeRef.detectChanges()
	}

	onSelected(){
		console.log("boe")
		console.log(this.selectedOption)
		//this.router.navigate('#channel/'+this.selectedOption);
		this.dataService.currentChannelID=parseInt(this.selectedOption);
	} 

	save(){
		  this.dataService.save('channels', this.dataService.currentChannel)
	}


	



}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/