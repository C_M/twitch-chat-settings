/* inspired on the heroes angular example */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { TodoItem } from './todo-item';
import { TodoItemsOnDate } from './todo-item';

import { MessageService } from './message.service';
import { Channel } from './model/channel';
import { DataService } from './data.service'


@Injectable({ providedIn: 'root' })
export class DatabaseService {

	//private apiUrl = 'http://localhost:8080';  // URL to web api
	// private apiUrl = 'api/';

	// httpOptions = {
	//   headers: new HttpHeaders({ 'Content-Type': 'application/json'})
	// };

	indexedDB = window.indexedDB;
	request : IDBOpenDBRequest;
	name = 'c_m_twitch_chat';
	version = 2.0;
	_ready = false;


	constructor(private messageService: MessageService, private dataService: DataService) {
				//let db: {[key:string]:any} = {}
			
		console.log(this.indexedDB)
		this.request = indexedDB.open(this.name, this.version);
		this.request.onsuccess = function(){
			console.log("opened")
		}
		this.request.onerror = function (event) {
			console.error("An error occurred with IndexedDB");
			console.error(event);
			window.alert("something went wrong with local database, please reload page")
		};

		const self = this
		this.request.onupgradeneeded = function (upgradeDB) {
			console.log("UPGRADE")
			const db_temp = self.request.result;
			switch(upgradeDB.oldVersion){
				case 0: 
					const users = db_temp.createObjectStore("users", { keyPath: ['roomID', 'userID'] });
					users.createIndex("roomID", "roomID", { unique: false });
					users.createIndex("userID", "userID", {unique:false});
					users.createIndex("nameLowercase", "nameLowercase", {unique:true});
					const channels = db_temp.createObjectStore("channels", { keyPath: 'id' });
					channels.createIndex('name', 'name', {unique: true})
			}
		}     
	}

	open():void{


	}

	get db(){
		//console.log(["hai", this.request])

		console.log(["hai", this.request])
		return this.request;
	}

	// getAllChannel(){
		
	// 	if(this.db){
	// 		const channels:Channel[] = this.db.transaction("channels", 'readonly').objectStore("channels");
	// 		let query = channels.getAll()
	// 		query.onsuccess = function(){
	// 			console.log(query.result)
	// 			this.dataService.channels = query.result
	// 		}
	// 	}

	// }

	getChannelUsersByPromise(roomID:number){
		const db = this.db
		return new Promise((resolve) => 
			{
				if(this.db==null){
					return
				}
				const users = db.transaction("channels", 'readonly').objectStore("channels");
				let request = channel.get(id)

				request.onsuccess = function() {
					let res = request.result;
					resolve(res);
				};
				request.onerror = function() {
					let res = request.result;
					resolve(null);
				};
			}
		);			
	}

	getUserByName(name:string){
		// const users = db.transaction("users", 'readonly').objectStore("users");
		// const lc_index = users.index("nameLowercase")
		// console.log(name)s
		// let request = lc_index.get(name.toLowerCase())
		//   request.onsuccess = function () {
		//   	console.log("get db succes")
		//     console.log(request.result)
		//     }
	}

	helloWorld():void{
	console.log("HELLO WORLD")
	}

  // /** GET items from the server */
  // getItems(): Observable<TodoItem[]> {
  //   const url = `${this.apiUrl}list`;
  //   return this.http.get<TodoItem[]>(url)
  //     .pipe(
  //       tap(_ => this.log('fetched items')),
  //       catchError(this.handleError<TodoItem[]>('getItems()', []))
  //     );
  // }

  // getItemsListByDate(subquery: String): Observable<TodoItemsOnDate[]> {
  //   const url = `${this.apiUrl}listbydate/${subquery}`;
  //   return this.http.get<TodoItemsOnDate[]>(url)
  //     .pipe(
  //       tap(_ => this.log('fetched items')),
  //       catchError(this.handleError<TodoItemsOnDate[]>('getItemsBydate()', []))
  //     );
  // }

  // /** GET todo-item by id. Will 404 if id not found */
  // getItem(id: number): Observable<TodoItem> {
  //   const url = `${this.apiUrl}item?id=${id}`;
  //   return this.http.get<TodoItem>(url).pipe(
  //     tap(_ => this.log(`fetched item id=${id}`)),
  //     catchError(this.handleError<TodoItem>(`Item id=${id}`))
  //   );
  // }


  // //////// Save methods //////////

  // /** DELETE: delete the item from the server */
  // deleteItem(id: number): Observable<TodoItem> {
  //   const url = `${this.apiUrl}delete?id=${id}`;

  //   return this.http.delete<TodoItem>(url, this.httpOptions).pipe(
  //     tap(_ => this.log(`deleted item id=${id}`)),
  //     catchError(this.handleError<TodoItem>('deleteItem'))
  //   );
  // }

  // /** PUT: update the item on the server */
  // updateItem(todo_item: TodoItem): Observable<any> {
  //   const url = `${this.apiUrl}/update`;
  //   return this.http.put(url, todo_item, this.httpOptions).pipe(
  //     tap(_ => this.log(`updated item id=${todo_item.id}`)),
  //     catchError(this.handleError<any>('updateItem'))
  //   );
  // }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
	return (error: any): Observable<T> => {

	  // TODO: send the error to remote logging infrastructure
	  console.error(error); // log to console instead

	  // TODO: better job of transforming error for user consumption
	  this.log(`${operation} failed: ${error.message}`);

	  // Let the app keep running by returning an empty result.
	  return of(result as T);
	};
  }

  /** Log a TodoItemService message with the MessageService */
  private log(message: string) {
	this.messageService.add(`ItemService: ${message}`);
  }
}
