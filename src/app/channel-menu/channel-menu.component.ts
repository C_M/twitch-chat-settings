import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DataService } from '../data.service';
import { Channel } from '../model/channel'

//import { ItemService } from '../items.service';

@Component({
  selector: 'app-channel-menu',
  templateUrl: './channel-menu.component.html',
  styleUrls: [ './channel-menu.component.css' ]
})
export class ChannelMenuComponent implements OnInit {
  channel: Channel | null = null
  @Input() 
  channelID:number = NaN;

  menuList: string[][]  = [['General Settings', 'general'], 
                                      ['Group Permissions', 'permissions'],
                                      ['Users', 'users'],
                                       ['CSS', 'css'],
                                        ['Export', 'export'],
                                      ]

  constructor(
    private route: ActivatedRoute,
    //private itemService: ItemService,
    private location: Location,
    private dataService: DataService
  ) {}

  ngOnInit(){
    const self = this
    this.dataService.subscribe('currentChannelID', this.currChannelsUpdated.bind(this))


  
  }

  currChannelsUpdated(prop:string, val:number){
    console.log("got update cuur chan")
    this.channelID=val;
    this.channel = this.dataService.currentChannel
   
    
  }


  // getItemsList(): void{
  //   console.log("getting items");
  //   this.itemService.getItems().subscribe(todo_items => this.todo_items = todo_items);
  // }

  // getItemsListByDate(selection: string): void{
  //   console.log("getting items: " + selection);
  //   this.show = selection;

  //   this.itemService.getItemsListByDate(selection).subscribe(
  //     todo_items_by_date => this.todo_items_by_date = todo_items_by_date);
  // }

  // update(selection: string): void{
  //   this.getItemsListByDate(selection);

  // }

}
