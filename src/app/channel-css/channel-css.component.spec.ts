import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelCssComponent } from './channel-css.component';

describe('ChannelCssComponent', () => {
  let component: ChannelCssComponent;
  let fixture: ComponentFixture<ChannelCssComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelCssComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChannelCssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
