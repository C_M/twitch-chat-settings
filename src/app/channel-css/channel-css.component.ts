import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { Channel, GeneralSettingsDescription, ChannelSettings, generalSettingsDescriptionList, generalSettingsDescriptionListID } from '../model/channel'

@Component({
  selector: 'app-channel-css',
  templateUrl: './channel-css.component.html',
  styleUrls: ['./channel-css.component.css']
})
export class ChannelCssComponent implements OnInit {

@Input() channel: Channel | null = null;
  generalSettings = generalSettingsDescriptionList;
  generalSettingsID = generalSettingsDescriptionListID;


  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  save(){
    this.dataService.save('channels', this.channel)
  }

   OnChanges(){
    console.log("changed, added dataservice")
    this.dataService.dataChanged()
    }


    
  CMinputChange(){
    this.OnChanges()
  }


}
