import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Channel, channelData } from '../model/channel'
import { ActivatedRoute } from '@angular/router'


@Component({
  selector: 'app-channel-import',
  templateUrl: './channel-import.component.html',
  styleUrls: ['./channel-import.component.css']
})
export class ChannelImportComponent implements OnInit {
	channel:any
	channelID:number = NaN
	channelData:channelData = new channelData()

  constructor(private dataService:DataService, private route: ActivatedRoute,
) { }

  ngOnInit(): void {
	this.channelID = parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
  	this.dataService.subscribe('currentChannelID', this.updateCurrentChannelID.bind(this))
    this.channel = this.dataService.currentChannel 
    this.channelData.channels = [this.channel]  
    this.dataService.subscribe('currentChannel', this.currentChannelUpdated.bind(this))
    

    this.route.paramMap
      .subscribe(params => {
	        this.dataService.currentChannelID = 
		        parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
			})
    
    if(this.channelID){
      this.dataService.currentChannelID = this.channelID;
    }
    this.channelID = this.dataService.currentChannelID;
  }

  updateCurrentChannelID(prop:string, val:number){
    console.log("update curr channel")
    this.channelID=val
  }

  currentChannelUpdated(prop:string, val:Channel):void{
    this.channel = val;
    this.channelData.channels = [this.channel]
    console.log(this.channelData)
  }

	importFromTextarea() {
		//@ts-ignore
		let textareaData = document.getElementById("textareaImport").value;
		//@ts-ignore
		const data = JSON.parse(textareaData)
		if(data.channels){
			for(let i=0; i<data.channels.length; ++i){
				const channel = data.channels[i]
				this.dataService.save('channels', channel)
			}
		}
		console.log(data)
	}

// function to create json file from js object/dict and download it
	loadFile(){

	}


}
