import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelImportComponent } from './channel-import.component';

describe('ChannelImportComponent', () => {
  let component: ChannelImportComponent;
  let fixture: ComponentFixture<ChannelImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelImportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChannelImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
