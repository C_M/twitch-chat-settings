import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelPermissionsComponent } from './channel-permissions.component';

describe('ChannelPermissionsComponent', () => {
  let component: ChannelPermissionsComponent;
  let fixture: ComponentFixture<ChannelPermissionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelPermissionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChannelPermissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
