import { Component, OnInit, Input } from '@angular/core';
import { Channel, generalSettingsDescriptionList, generalSettingsDescriptionListID } from '../model/channel'
import { TYPE } from '../types'
import { DataService } from '../data.service'

@Component({
  selector: 'app-channel-permissions',
  templateUrl: './channel-permissions.component.html',
  styleUrls: ['./channel-permissions.component.css']
})
export class ChannelPermissionsComponent implements OnInit {

  STRING=TYPE.STRING
  BOOLEAN=TYPE.BOOLEAN
  NUMBER=TYPE.NUMBER
  STRINGARRAY=TYPE.STRINGARRY

	 @Input() channel:Channel | null = null
   permissions = {}
   permissionsList = generalSettingsDescriptionList[generalSettingsDescriptionListID['permission']]

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  	console.log("=== PERMISSIONS ===")
  	console.log(this.channel)
   
  }


   CMinputChange(){
    this.OnChanges()
  }

  OnChanges(){
    console.log("changed, added dataservice")
    this.dataService.dataChanged()
    }


}
