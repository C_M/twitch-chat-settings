import { Component, OnInit, Input } from '@angular/core';
import { Channel } from '../model/channel';
import { DataService } from '../data.service'

@Component({
  selector: 'app-channel-selector',
  templateUrl: './channel-selector.component.html',
  styleUrls: ['./channel-selector.component.css']
})
export class ChannelSelectorComponent implements OnInit {
	@Input() channels:Channel[] =[]

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  	this.dataService.subscribe('channels', this.channelsUpdated.bind(this))
  	this.channels = this.dataService.channels;
  }

  	channelsUpdated(prop:string, val:Channel[]){
		this.channels=val;

		
		//this.valueChange.emit(this.channels)
		//() => this.valueChange.emit(this.channels);
	//	this.changeRef.detectChanges()
	}

}
