import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';

import { TopBarComponent } from './topbar/topbar.component';


import { NgxIndexedDBModule, DBConfig } from 'ngx-indexed-db';
import { DataService } from './data.service'
import { ChannelMenuComponent } from './channel-menu/channel-menu.component'
import { UserComponent } from './user/user.component'
import { ChannelComponent } from './channel/channel.component';
import { ChannelExportComponent } from './channel-export/channel-export.component';
import { ChannelImportComponent } from './channel-import/channel-import.component';
import { ChannelSelectorComponent } from './channel-selector/channel-selector.component';
import { ChannelPermissionsComponent } from './channel-permissions/channel-permissions.component';
import { ChannelCssComponent } from './channel-css/channel-css.component'


//import { TodoListItemComponent } from './todo-list-item/todo-list-item.component';

//Ahead of time compiles requires an exported function for factories
export function migrationFactory() {
  // The animal table was added with version 2 but none of the existing tables or data needed
  // to be modified so a migrator for that version is not included.
  return {
    0: (db:any, transaction:any) => {
      const store = transaction.objectStore('people');
      store.createIndex('country', 'country', { unique: false });
    },
    1: (db:any, transaction:any) => {
      const store = transaction.objectStore('people');
      store.createIndex('age', 'age', { unique: false });
    }
  };
}

const dbConfig: DBConfig  = {
  name: 'c_m_twitch_chat',
  version: 2,
  objectStoresMeta: [{
    store: 'channels',
    storeConfig: { keyPath: 'id', autoIncrement: false },
    storeSchema: [
      { name: 'name', keypath: 'name', options: { unique: true } }
    ]
  }, {

    store: 'users',
    storeConfig: {keyPath: ['roomID', 'userID'], autoIncrement: false },
    storeSchema: [
      { name: 'roomID', keypath: 'roomID', options: { unique: false } },
      { name: 'userID', keypath: 'userID', options: { unique: false } },
      { name: 'nameLowercase', keypath: 'nameLowercase', options: { unique: true } },
    ]
  },
  ],
  // provide the migration factory to the DBConfig
  migrationFactory
};


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    ChannelMenuComponent,
    UserComponent,
    ChannelComponent,

   // TodoListItemComponent,
    MessagesComponent,
    
    ChannelExportComponent,
         ChannelImportComponent,
         ChannelSelectorComponent,
         ChannelPermissionsComponent,
         ChannelCssComponent
  ],
  imports: [
    BrowserModule,
    NgxIndexedDBModule.forRoot(dbConfig) ,
    AppRoutingModule,
   // HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
