import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component'
import { ChannelComponent } from './channel/channel.component'
import {ChannelExportComponent} from './channel-export/channel-export.component'

import {ChannelImportComponent} from './channel-import/channel-import.component'
//import {TodoListItemComponent} from './todo-list-item/todo-list-item.component'


const routes: Routes = [
	//  { path: '', redirectTo: 'dashboard/all', pathMatch: 'full' },
	//  { path: 'dashboard', redirectTo: 'dashboard/all', pathMatch: 'full' },
	 /* { path: 'new', redirectTo: '/detail/-1', pathMatch: 'full' },
	  { path: 'dashboard/:selection', component: TodoListComponent },
	  { path: 'detail/:id', component: TodoListItemComponent },
	  { path: 'detail/:id', component: TodoListItemComponent },
	  { path: 'edit/:id', component: TodoListItemComponent },
	  { path: 'view/:id', component: ViewItemComponent }*/
	  { path: 'index.html', component: ChannelComponent},
	  { path: '', redirectTo: 'index.html', pathMatch: 'full' },
	  { path: 'channel', component: ChannelComponent},
	  { path: 'channel/:channelID', component: ChannelComponent },
	  { path: 'channel/:channelID/export', component: ChannelExportComponent },
	  { path: 'import', component: ChannelImportComponent },
	  { path: 'channel/:channelID/users', component: UserComponent },
	  { path: 'channel/:channelID/:settingsPart', component: ChannelComponent },
	  { path: 'channel/:channelID/:settingsPart', component: ChannelComponent },
	  
	  { path: 'channel/:channelID/user:userID', component: UserComponent },

	];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
