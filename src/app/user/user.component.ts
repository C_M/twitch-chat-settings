import { Component, OnInit, Input, Output, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DataService } from '../data.service';
import { Channel } from '../model/channel'
import { User } from '../model/user'

//import { ItemService } from '../items.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: [ './user.component.css' ]
})
export class UserComponent implements OnInit {
  userlist: User[] = [] 
  userDict: {[key:number]:User} = {}
  @Input() currentChannelID:number = NaN;
  userID : number | null = null

  constructor(
    private route: ActivatedRoute,
    //private itemService: ItemService,
    private location: Location,
    private dataService: DataService
  ) {

    this.dataService.subscribe('currentChannelID', this.updateCurrentChannelID.bind(this))

  }

  ngOnInit(){
    console.log("USERPAGEEE")
    const id = parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
    this.userID= parseInt(this.route.snapshot.paramMap.get('userID')!, 10);
    this.currentChannelID = id;
    this.route.paramMap
      .subscribe(userID => {
        console.log(userID)
        let newID = (parseInt(this.route.snapshot.paramMap.get('channelID')!, 10))
        this.userID= (parseInt(this.route.snapshot.paramMap.get('userID')!, 10))
        this.dataService.currentChannelID=newID;
        console.log("UPDATE channel id")
  
    });
    if(id){
      this.dataService.currentChannelID = id;
    }
    this.currentChannelID = this.dataService.currentChannelID;
    this.getUserlist();
  }

  async getUserlist(){
    
    this.userlist = await this.dataService.getUserlist() 
    console.log("user.html userlist")
    this.userlist.sort(function(a,b){ return a.name.localeCompare(b.name)});
    this.userDict = {}

    for(let i = 0; i < this.userlist.length; ++i){
      this.userDict[this.userlist[i].userID] = this.userlist[i];
    }
    console.log(["after::: request userlist", this.dataService._currentChannelId])
    console.log(this.userDict)
  }

  updateCurrentChannelID(prop:string, val:number){
    console.log("update curr channel")
    this.currentChannelID=val
    this.getUserlist();
  }



  // getItemsList(): void{
  //   console.log("getting items");
  //   this.itemService.getItems().subscribe(todo_items => this.todo_items = todo_items);
  // }

  // getItemsListByDate(selection: string): void{
  //   console.log("getting items: " + selection);
  //   this.show = selection;

  //   this.itemService.getItemsListByDate(selection).subscribe(
  //     todo_items_by_date => this.todo_items_by_date = todo_items_by_date);
  // }

  // update(selection: string): void{
  //   this.getItemsListByDate(selection);

  // }

}
