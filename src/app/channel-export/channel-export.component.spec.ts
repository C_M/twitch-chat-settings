import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelExportComponent } from './channel-export.component';

describe('ChannelExportComponent', () => {
  let component: ChannelExportComponent;
  let fixture: ComponentFixture<ChannelExportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelExportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChannelExportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
