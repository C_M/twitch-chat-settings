import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Channel, channelData } from '../model/channel'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-channel-export',
  templateUrl: './channel-export.component.html',
  styleUrls: ['./channel-export.component.css']
})

export class ChannelExportComponent implements OnInit {
	channel:any
	channelID:number = NaN
	channelData:channelData = new channelData()

  constructor(private dataService:DataService, private route: ActivatedRoute,
) { }

  ngOnInit(): void {
	this.channelID = parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
  	this.dataService.subscribe('currentChannelID', this.updateCurrentChannelID.bind(this))
    this.channel = this.dataService.currentChannel 
    this.channelData.channels = [this.channel]  
    this.dataService.subscribe('currentChannel', this.currentChannelUpdated.bind(this))
    

    this.route.paramMap
      .subscribe(params => {
	        this.dataService.currentChannelID = 
		        parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
			})
    
    if(this.channelID){
      this.dataService.currentChannelID = this.channelID;
    }
    this.channelID = this.dataService.currentChannelID;
  }

  updateCurrentChannelID(prop:string, val:number){
    console.log("update curr channel")
    this.channelID=val
  }

  currentChannelUpdated(prop:string, val:Channel):void{
    this.channel = val;
    this.channelData.channels = [this.channel]
    console.log(this.channelData)
  }

	copyTextarea() {
		let textarea = document.getElementById("textareaExport");
		//@ts-ignore
		textarea.select();
		document.execCommand("copy");
		//@ts-ignore
		document.getElementById("copyClipboardButton").value = "saved"
	}

// function to create json file from js object/dict and download it
	exportToFile(){
		let json = JSON.stringify(this.channelData, null, 2)
		let uri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(json)
		let element = document.createElement('a')
		element.setAttribute('href', uri)
		element.setAttribute('download', 'channel_'+this.channel.name+"_"+
			this.channel.id+".json")
		element.click()
	}


}
