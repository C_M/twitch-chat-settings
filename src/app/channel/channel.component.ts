import { Component, OnInit, Input, Output, OnChanges, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule,FormBuilder, ReactiveFormsModule, FormArray, FormGroup, FormControl } from '@angular/forms';
import { TYPE } from '../types'


import { DataService } from '../data.service';
import { Channel, GeneralSettingsDescription, ChannelSettings, generalSettingsDescriptionList, generalSettingsDescriptionListID } from '../model/channel'
import { User } from '../model/user'


//import { ItemService } from '../items.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: [ './channel.component.css' ]
})
export class ChannelComponent implements OnInit {


  currentChannelID:number = NaN;
   @Input() channel:Channel | null = null
   //chSettings: {[key:string]:string} = {}

  userID : number | null = null
  settingsPart: string | null = null;

  generalSettings = generalSettingsDescriptionList;
  generalSettingsID = generalSettingsDescriptionListID;

  STRING=TYPE.STRING
  BOOLEAN=TYPE.BOOLEAN
  NUMBER=TYPE.NUMBER
  STRINGARRAY=TYPE.STRINGARRY

   constructor(
    private route: ActivatedRoute,
    //private itemService: ItemService,
    private location: Location,
    private dataService: DataService,
    private formBuilder:FormBuilder
  ) {

    this.dataService.subscribe('currentChannelID', this.updateCurrentChannelID.bind(this))
    this.channel = this.dataService.currentChannel
   
    this.dataService.subscribe('currentChannel', this.currentChannelUpdated.bind(this))

  }

   CMinputChange(){
    this.OnChanges()
  }

  OnChanges(){
    console.log("changed, added dataservice")
    this.dataService.dataChanged()
    }

// function to store values in array after field is left (to prevent reloading during typing)
  onArrayFormBlur(settingArr:string[], i:number, elID:any){
    // @ts-ignore
    const val:string = document.getElementById(elID)?.value || ""
    console.log([settingArr, i, val])
    settingArr[i]=val;
  //  window.alert("hallo")
  }

  ArryFormAddItem(settingArr:string[]){
    settingArr.push("New")
  }

   ArryFormDelItem(settingArr:string[], index:number){
    settingArr.splice(index, 1); 
  }

  ngOnInit(){
    console.log(["settingspage", this.settingsPart])
    const id = parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
    this.settingsPart = this.route.snapshot.paramMap.get('settingsPart');
    console.log(["settingspage", this.settingsPart])
    this.currentChannelID = id;
    this.route.paramMap
      .subscribe(params => {
        this.settingsPart = this.route.snapshot.paramMap.get('settingsPart');});
      this.dataService.currentChannelID = parseInt(this.route.snapshot.paramMap.get('channelID')!, 10);
    
    if(id){
      this.dataService.currentChannelID = id;
    }
    this.currentChannelID = this.dataService.currentChannelID;

  }

  save(){
    this.dataService.save('channels', this.channel)
  }



  updateCurrentChannelID(prop:string, val:number){
    console.log("update curr channel")
    this.currentChannelID=val
    console.log( this.currentChannelID)
  }

  currentChannelUpdated(prop:string, val:Channel):void{

    this.channel = val;
    console.log("00000")
    console.log(this.channel )
  }


}
